package com.vm.model.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.vm.common.enumeration.machine.Status;

@Entity
@Table(name = "machine")
public class Machine {

	@Id
	@Column(name = "id")
	@GeneratedValue()
	private Long id;

	@Column(name = "uid")
	private String uid;
	
	@Column(name = "status")
	private Status status;

	@Column(name = "createdBy")
	private Long createdBy;

	@Column(name = "createdAt")
	private Long createdAt;

	@Column(name = "active")
	private boolean active;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Machine [id=" + id + ", uid=" + uid + ", status=" + status + ", createdBy=" + createdBy + ", createdAt="
				+ createdAt + ", active=" + active + "]";
	}


}
