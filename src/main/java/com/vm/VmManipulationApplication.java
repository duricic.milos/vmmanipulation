package com.vm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VmManipulationApplication {

	public static void main(String[] args) {
		SpringApplication.run(VmManipulationApplication.class, args);
	}

}
