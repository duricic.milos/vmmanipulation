package com.vm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vm.model.core.Machine;
import com.vm.service.MachineService;

@Transactional
@Service
public class MachineServiceImpl implements MachineService{

	@Autowired
	private MachineRepository machineRepository;
	
	@Override
	public Machine save(Machine machine) {
		return machineRepository.save(machine);
	}
	
	@Override
	public List<Machine> findAll() {
		return machineRepository.findAll();
	}

	@Override
	public Machine findOne(String uid) {
		return machineRepository.findByUid(uid);
	}

	
	
}
