package com.vm.service.impl;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vm.model.core.Machine;

public interface MachineRepository extends JpaRepository<Machine, Long> {
	
	Machine findByUid(String uid);

}
