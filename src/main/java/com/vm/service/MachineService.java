package com.vm.service;

import java.util.List;

import com.vm.model.core.Machine;

public interface MachineService {

	Machine save(Machine machine);

	Machine findOne(String uid);
	
	List<Machine> findAll();
	
}
