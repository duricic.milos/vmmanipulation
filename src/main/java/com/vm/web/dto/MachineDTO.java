package com.vm.web.dto;

import com.vm.common.enumeration.machine.Status;
import com.vm.model.core.Machine;

public class MachineDTO {

	private String uid;
	private Status status;
	private Long createdBy;
	private Long createdAt;
	private boolean active;

	public MachineDTO() {
		super();
	}
	
	public MachineDTO(Machine machine) {
		this.uid = machine.getUid();
		this.status = machine.getStatus();
		this.createdBy = machine.getCreatedBy();
		this.createdAt = machine.getCreatedAt();
		this.active = machine.getActive();
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "MachineDTO [uid=" + uid + ", status=" + status + ", createdBy=" + createdBy + ", createdAt=" + createdAt
				+ ", active=" + active + "]";
	}
	
}
