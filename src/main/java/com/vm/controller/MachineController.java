package com.vm.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vm.common.enumeration.machine.Status;
import com.vm.exception.InvalidTokenException;
import com.vm.jwt.JwtUtil;
import com.vm.model.core.Machine;
import com.vm.service.MachineService;
import com.vm.service.OurUserDetailsService;
import com.vm.web.dto.MachineDTO;

@RestController
@RequestMapping(value = "machine")
public class MachineController {
	
	@Autowired
	private MachineService machineService;

//	@Autowired
//	private JwtUtil jwtUtil;
//
//	@Autowired
//	private OurUserDetailsService userDetailsService;

	@GetMapping
	public ResponseEntity<List<MachineDTO>> getMachines( //
//			@RequestParam String name, //
//			@RequestParam String status, //
//			@RequestParam String dateFrom, //
//			@RequestParam String dateTo //
	) {

		List<MachineDTO> vmsDTO = new ArrayList<MachineDTO>();
		List<Machine> vms = machineService.findAll();
		for (Machine machine : vms) {
			vmsDTO.add(new MachineDTO(machine));
		}
		
		return ResponseEntity.ok(vmsDTO);
	}
	
	@PostMapping("create")
	ResponseEntity<MachineDTO> createMachine( //
			@RequestHeader(name = "Authorization") String jwt //
			) throws InvalidTokenException {
//		final UserDetails userDetails = userDetailsService //
//				.loadUserByUsername(jwtUtil.extractUsername(jwt));
		Machine machine = new Machine();
		machine.setUid(UUID.randomUUID().toString());
		machine.setStatus(Status.STOPPED);
		//to implement get userId by username
		machine.setCreatedBy(1L);
		machine.setCreatedAt(System.currentTimeMillis() / 1000L);
		machine.setActive(true);
		Machine machineSaved = machineService.save(machine);
		
		return ResponseEntity.ok(new MachineDTO(machineSaved));
	}
	
	@PostMapping("start")
	ResponseEntity<MachineDTO> startMachine( //
			@RequestHeader(name = "Authorization") String jwt //
			,@RequestParam(name = "uid") String uid
	) {
		Machine machine = machineService.findOne(uid);
		if(machine != null && machine.getStatus().equals(Status.STOPPED)) {
			machine.setStatus(Status.RUNNING);
			
			Machine machineSaved = machineService.save(machine);
			
			return ResponseEntity.ok(new MachineDTO(machineSaved));
		}
		
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	@PostMapping("stop")
	ResponseEntity<MachineDTO> stopMachine( //
			@RequestHeader(name = "Authorization") String jwt //
			,@RequestParam(name = "uid") String uid
			) {
		Machine machine = machineService.findOne(uid);
		if(machine != null && machine.getStatus().equals(Status.RUNNING)) {
			machine.setStatus(Status.STOPPED);
			
			Machine machineSaved = machineService.save(machine);
			
			return ResponseEntity.ok(new MachineDTO(machineSaved));
		}
		
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	@PostMapping("restart")
	ResponseEntity<MachineDTO> restartMachine( //
			@RequestHeader(name = "Authorization") String jwt //
			,@RequestParam(name = "uid") String uid
			) {
		Machine machine = machineService.findOne(uid);
		if(machine != null && machine.getStatus().equals(Status.RUNNING)) {
			machine.setStatus(Status.STOPPED);
			machine.setStatus(Status.RUNNING);
			
			Machine machineSaved = machineService.save(machine);
			
			return ResponseEntity.ok(new MachineDTO(machineSaved));
		}
		
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping("destroy")
	ResponseEntity<MachineDTO> destroyMachine( //
			@RequestHeader(name = "Authorization") String jwt //
			,@RequestParam(name = "uid") String uid
			) {
		Machine machine = machineService.findOne(uid);
		if(machine != null && machine.getStatus().equals(Status.STOPPED)) {
			machine.setActive(false);
			
			Machine machineSaved = machineService.save(machine);
			
			return ResponseEntity.ok(new MachineDTO(machineSaved));
		}
		
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

}
